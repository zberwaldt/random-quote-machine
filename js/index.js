// Create an area to hold objects of quotes
var quotes = [
  {
    quotee: "Confucius",
    quote: "Life is really simple, but we insist on making it complicated"
  },
  {
    quotee: "Confucius",
    quote: "It does not matter how slowly you go as long as you do not stop"
  },
  {
    quotee: "Carol Burnett",
    quote: "Only I can change my life. No one can do it for me."
  },
  {
    quotee: "St. Jerome",
    quote: "Good, better, best. Never let it rest. 'Til your good is better and your better is best."
  },
  {
    quotee: "Nelson Mandela",
    quote: "It always seems impossible until it's done."
  },
  {
    quotee: "Jom Rohn",
    quote: "Happiness is not something you postpone for the future; it is somethint you design for the present."
  },
  {
    quotee: "Richard Bach",
    quote: "The best way to pay for a lovely moment is to enjoy it."
  },
  {
    quotee: "Michael J. Fox",
    quote: "Family is not an important thing. It's everything."
  },
  {
    quotee: "Unknown",
    quote: "I have to go to the bathroom."
  },
  {
    quotee: "Winston Churchill",
    quote: "I may be drunk, Miss, but in the morning I will be sober and you will still be ugly."
  },
  {
    quotee: "Zach Berwaldt",
    quote: "I coded this myself"
  }
]
  // Get element where the quoted person will go
  var quoted = document.getElementById('quotee');
  // Get element where the quote will go
  var quote = document.getElementById('quote-content');
  // Get Twitter Share Button
  var tweet = document.getElementById("twitter-share-button");
  // Get the value of both the quote and quoted and concantonate them together as a string;
  var currentQuote = '"' + quote.innerHTML + '"  - ' + quoted.innerHTML;

  // Make sure that the twitter share button will tweet out the default quote 
  tweet.setAttribute('href', 'https://twitter.com/intent/tweet?hashtags=quotes&related=freecodecamp&text=' + encodeURIComponent(currentQuote));
  
  // Create a function that will generate a random number and use it to pick a new quote. 
function rNG() {
  
  // Generate a random number that will always be a whole number and always a number within the range of the array
  var randomNum = Math.floor(Math.random() * (quotes.length));
  // Give the chosen quotee to the corresponding HTML element
  quoted.innerHTML = quotes[randomNum].quotee;
  // Give the chosen quote to the corresponding HTML element
  quote.innerHTML = quotes[randomNum].quote;
  // Update the current quote
  currentQuote = '"' + quotes[randomNum].quote + '"  - ' + quotes[randomNum].quotee;
  // Update the href attribute of the twitter share button
  tweet.setAttribute('href', "http://twitter.com/share?text=" + currentQuote); 
 }